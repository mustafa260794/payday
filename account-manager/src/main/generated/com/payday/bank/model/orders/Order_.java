package com.payday.bank.model.orders;

import com.payday.bank.common.model.Account;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Order.class)
public abstract class Order_ {

	public static volatile SingularAttribute<Order, Double> filledPrice;
	public static volatile SingularAttribute<Order, Date> created;
	public static volatile SingularAttribute<Order, Long> numberOfShares;
	public static volatile SingularAttribute<Order, String> stockId;
	public static volatile SingularAttribute<Order, Long> id;
	public static volatile SingularAttribute<Order, Double> maxPrice;
	public static volatile SingularAttribute<Order, String> type;
	public static volatile SingularAttribute<Order, Date> updated;
	public static volatile SingularAttribute<Order, Account> account;
	public static volatile SingularAttribute<Order, OrderStatus> status;

	public static final String FILLED_PRICE = "filledPrice";
	public static final String CREATED = "created";
	public static final String NUMBER_OF_SHARES = "numberOfShares";
	public static final String STOCK_ID = "stockId";
	public static final String ID = "id";
	public static final String MAX_PRICE = "maxPrice";
	public static final String TYPE = "type";
	public static final String UPDATED = "updated";
	public static final String ACCOUNT = "account";
	public static final String STATUS = "status";

}

