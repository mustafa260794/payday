package com.payday.bank;

import com.payday.bank.common.config.LoggingTcpConfiguration;
import com.payday.bank.common.config.RedisConfiguration;
import com.payday.bank.exceptions.AlreadyExistException;
import com.payday.bank.security.PersonRepository;
import com.payday.bank.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Slf4j
@SpringBootApplication
@EnableCaching
@EnableJpaRepositories(
        basePackages = {"com.payday.bank.repository"}
)
@Import({RedisConfiguration.class, LoggingTcpConfiguration.class})
public class AccountManagerApplication implements CommandLineRunner {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private AccountService accountService;

    public static void main(String[] args) {
        SpringApplication.run(AccountManagerApplication.class, args);
    }


    @Override
    public void run(String... args) {
        log.trace("Creating demo accounts for the users");
        personRepository.getAllPersonNames()
                .stream()
                .forEach(this::createAccount);
    }

    private void createAccount(String u) {
        try {
            accountService.createAccount(u);
        } catch (AlreadyExistException e) {
            log.trace("Account is already exist, ignore...");
        }
    }
}
