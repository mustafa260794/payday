package com.payday.bank.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = Stock.TABLE_NAME, indexes = {
        @Index(columnList = Stock.NAME_COLUMN, name = Stock.NAME_INDEX, unique = true),
     //   @Index(columnList = Stock.STOCK_COLUMN, name = Stock.STOCK_INDEX, unique = true)
})
@Data
public class Stock {

    public static final String TABLE_NAME = "STOCKS";
    public static final String INDEX = "_index";
    public static final String NAME_COLUMN = "name";
    public static final String NAME_INDEX = NAME_COLUMN + INDEX;
    public static final String STOCK_COLUMN = "stock_id";
    public static final String STOCK_INDEX = STOCK_COLUMN + INDEX;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String stockId;

    private String name;

    private String index;

    private Double open;

    private Double current;

    private Double close;
}
