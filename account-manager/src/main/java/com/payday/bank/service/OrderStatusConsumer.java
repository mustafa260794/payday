package com.payday.bank.service;

import com.payday.bank.common.dto.OrderStatusDto;
import com.payday.bank.dto.OrderDto;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class OrderStatusConsumer {

    private final KafkaTemplate<String, OrderStatusDto> orderStatusTemplate;
    private final OrderService orderService;
    private final TransitionService transitionService;

    public OrderStatusConsumer(KafkaTemplate<String, OrderStatusDto> orderStatusTemplate,
            OrderService orderService, TransitionService transitionService) {
        this.orderStatusTemplate = orderStatusTemplate;
        this.orderService = orderService;
        this.transitionService = transitionService;
    }

    /**
     * Listens to the kafka broker, and processes order status messages. Once a message is received, the order details
     * are located and transitioned to the target state (SUBMITTED, FILLED etc.)
     *
     * @param orderStatusDto: the status of the order in the exchange
     */
    @KafkaListener(containerFactory = "ordersStatusKafkaListenerContainerFactory", topics = "${kafka.topics.orders.status}", groupId = "${kafka.consumer.groupId.orders_statuses}")
    public void listen(OrderStatusDto orderStatusDto) {
        log.trace("Received message:" + orderStatusDto);
        final Optional<OrderDto> orderById = orderService.findOrderById(orderStatusDto.getId());
        orderById.map((orderDto) -> {
            orderDto.setFilledPrice(orderStatusDto.getFilledPrice());
            transitionService.transitionOrder(orderDto, orderStatusDto.getStatus());
            return orderDto;
        }).orElseThrow(() -> new RuntimeException("Order not found for given id: " + orderStatusDto.getId()));
    }

}
