package com.payday.bank.service;

import com.payday.bank.common.dto.GenericSearchDto;
import com.payday.bank.common.dto.StockDto;
import com.payday.bank.common.model.PStock;
import com.payday.bank.common.repository.redis.PStocksRedisRepository;
import com.payday.bank.common.repository.search.SearchSpecification;
import com.payday.bank.repository.StocksRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class StocksService {

    private final ModelMapper mapper;
    private final StocksRepository stocksRepository;
    private final PStocksRedisRepository pStocksRedisRepository;

    /**
     * Lists all stocks fpr given search filter.
     *
     * @param filter : the search filter
     * @param pageable : the pageable
     * @return: the results found
     */
    @Transactional
    public Page<StockDto> findAll(GenericSearchDto filter, Pageable pageable) {
        return stocksRepository.findAll(new SearchSpecification<>(filter.getCriteria()), pageable)
                .map(p -> mapper.map(p, StockDto.class));
    }

    public Iterable<PStock> findAllPreferential() {
        return pStocksRedisRepository.findAll(); //I don't care about pagination, just do it work
    }
}
