package com.payday.bank.transitions;

import com.payday.bank.dto.OrderDto;
import com.payday.bank.model.orders.OrderStatus;
import com.payday.bank.model.orders.OrderTransition;
import org.springframework.stereotype.Service;

@Service
public class Failed implements OrderTransition {

    public static final String NAME = "failed";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public OrderStatus getStatus() {
        return OrderStatus.FAILED;
    }

    @Override
    public void applyProcessing(OrderDto order) {
        //Do processing here
    }
}
