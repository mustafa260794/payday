package com.payday.bank.transitions;

import com.payday.bank.model.orders.OrderStatus;
import com.payday.bank.model.orders.OrderTransition;
import com.payday.bank.service.AccountService;
import com.payday.bank.dto.OrderDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Filled implements OrderTransition {

    public static final String NAME = "filled";

    private final AccountService accountService;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public OrderStatus getStatus() {
        return OrderStatus.FILLED;
    }

    @Override
    public void applyProcessing(OrderDto order) {
        //Do processing here
        accountService.applyOrder(order);
    }
}
