package com.payday.bank.common.dto;

import java.util.Set;
import lombok.Data;

@Data
public class AccountDto {

    private Long id;

    private UserDto user;

    private Double cashBalance;

    private Double totalBalance;

    private Set<HoldingDto> stocks;

}

