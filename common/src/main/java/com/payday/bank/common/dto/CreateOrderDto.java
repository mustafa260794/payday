package com.payday.bank.common.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.Data;

@Data
public class CreateOrderDto {

    private Long id;

    @NotNull
    @Positive
    private Double maxPrice;

    @NotNull
    @NotEmpty
    private String stockId;

    @NotNull
    @Positive
    private Long numberOfShares;

    @NotNull
    @NotEmpty
    private String type;

    private String user;
    private String hookURL;


}
