package com.payday.bank.common.dto;

import lombok.Data;

@Data
public class HoldingDto {

    private String stockId;
    private Long numberOfShares;
    private Double averagePrice;
}
