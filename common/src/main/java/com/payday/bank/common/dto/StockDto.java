package com.payday.bank.common.dto;

import lombok.Data;

@Data
public class StockDto {

    private String stockId;

    private String name;

    private String index;

    private Double open;

    private Double current;

    private Double close;
}
