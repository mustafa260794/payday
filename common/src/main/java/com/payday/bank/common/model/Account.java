package com.payday.bank.common.model;

import com.payday.bank.common.model.user.User;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@Entity
@Table(name = Account.TABLE_NAME, indexes = {
        @Index(columnList = Account.OWNER_COLUMN, name = Account.OWNER_INDEX, unique = true)
})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RedisHash(Account.TABLE_NAME)
public class Account {

    public static final String TABLE_NAME = "ACCOUNTS";
    public static final String INDEX = "_index";
    public static final String OWNER_COLUMN = "user_id";
    public static final String OWNER_INDEX = OWNER_COLUMN + INDEX;

    @Id
    @Indexed
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "cash_balance")
    private Double cashBalance; //Assume we support usd only

    @Column(name = "total_balance")
    private Double totalBalance; //Total balance including stocks

    @OneToMany(mappedBy = "accountId", fetch = FetchType.EAGER)
    private Set<Holding> stocks;

}
