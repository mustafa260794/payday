package com.payday.bank.common.model.user;

public enum UserRole {
    ADMIN,
    CUSTOMER
}
