package com.payday.bank.common.repository.redis;

import com.payday.bank.common.model.Account;
import com.payday.bank.common.model.user.User;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRedisRepository extends CrudRepository<Account, Long> {

    Optional<Account> findByUser(User user);
}
