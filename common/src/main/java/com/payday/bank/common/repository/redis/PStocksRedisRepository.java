package com.payday.bank.common.repository.redis;

import com.payday.bank.common.model.PStock;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PStocksRedisRepository extends CrudRepository<PStock, Long> {

    Optional<PStock> findByStockId(String stockId);

    List<PStock> findByPreferentialIsTrue();
}
