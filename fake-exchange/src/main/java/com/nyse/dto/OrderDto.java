package com.nyse.dto;

import com.nyse.model.orders.OrderStatus;
import java.util.Date;
import lombok.Data;

@Data
public class OrderDto {

    private Long id;

    private Long externalId;

    private String stockId;

    private Double maxPrice;

    private String hookURL;

    private Integer numberOfShares;

    private String type;

    private Double filledPrice;

    private OrderStatus status;

    private Date created;

    private Date updated;
}
