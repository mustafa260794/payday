package com.nyse.model.orders;

import java.util.Arrays;
import java.util.List;

public enum OrderStatus {

    NEW(Filled.NAME, Cancelled.NAME),
    FILLED(),
    CANCELLED();

    private final List<String> transitions;

    OrderStatus(String... transitions) {
        this.transitions = Arrays.asList(transitions);
    }

    public List<String> getTransitions() {
        return transitions;
    }

}
