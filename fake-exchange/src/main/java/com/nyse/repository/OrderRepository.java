package com.nyse.repository;

import com.nyse.model.orders.Order;
import com.nyse.model.orders.OrderStatus;
import java.util.List;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrderRepository extends PagingAndSortingRepository<Order, Long>, JpaSpecificationExecutor<Order> {

    List<Order> findByStockIdAndStatusAndMaxPriceLessThanAndType(String symbol, OrderStatus orderStatus,
            Double price, String type);

    List<Order> findByStockIdAndStatusAndMaxPriceGreaterThanAndType(String symbol, OrderStatus orderStatus,
            Double price, String type);
}
