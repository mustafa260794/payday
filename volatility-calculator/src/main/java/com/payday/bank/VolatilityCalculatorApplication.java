package com.payday.bank;

import com.payday.bank.common.config.LoggingTcpConfiguration;
import com.payday.bank.common.config.RedisConfiguration;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableAsync
@SpringBootApplication
@EnableScheduling
@EnableJpaRepositories(basePackages = "com.payday.repository")
@Import({RedisConfiguration.class, LoggingTcpConfiguration.class})
public class VolatilityCalculatorApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(VolatilityCalculatorApplication.class, args);
    }

    @Override
    public void run(String... args) {
    }

}
