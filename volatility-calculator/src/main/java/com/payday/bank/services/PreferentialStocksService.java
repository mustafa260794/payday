package com.payday.bank.services;

import com.payday.bank.common.dto.StockDto;
import com.payday.bank.common.model.PStock;
import com.payday.bank.common.repository.redis.PStocksRedisRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PreferentialStocksService {

    private final PStocksRedisRepository pStocksRedisRepository;

    public void processPreferentialStocks(StockDto stockDto) {
        final PStock pStock = pStocksRedisRepository.findByStockId(stockDto.getStockId())
                .orElse(createNewPreferentialStock(stockDto));
        updateMinMax(pStock, stockDto);
        isPreferential(pStock);
        pStocksRedisRepository.save(pStock);
    }

    private boolean isPreferential(PStock pStock) {
        if (pStock.getCurrent() < 3) { //penny stock
            return false;
        }
        if ((pStock.getMax() - pStock.getMin()) / (pStock.getMax() + pStock.getMin()) > 5) {
            // propbably this is the correct formula
            return false;
        }
        return true;
    }

    private PStock updateMinMax(PStock pStock, StockDto stockDto) {
        if (pStock.getMax() < stockDto.getCurrent()) {
            pStock.setMax(stockDto.getCurrent());
        }
        if (pStock.getMin() < stockDto.getCurrent()) {
            pStock.setMin(stockDto.getCurrent());
        }
        return pStock;
    }

    private PStock createNewPreferentialStock(StockDto stockDto) {
        PStock pStock = new PStock();
        pStock.setIndex(stockDto.getIndex());
        pStock.setOpen(stockDto.getOpen());
        pStock.setClose(stockDto.getClose());
        pStock.setCurrent(stockDto.getCurrent());
        pStock.setStockId(stockDto.getStockId());
        pStock.setMin(0.0);
        pStock.setMax(0.0);
        pStock.setPreferential(true);
        return pStock;
    }
}
